import Vue from 'vue';
import VueQueryBuilder from 'vue-query-builder';

Vue.use(VueQueryBuilder);
