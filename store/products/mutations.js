export default {
  setLoading(state, value) {
    state.isLoading = value;
  },
  setActiveMenu(state, value) {
    state.activeMenu = value;
  },
};
