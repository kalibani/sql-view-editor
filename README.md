# sql-view-editor

[SQL view editor](https://sql-view-editor.netlify.app/)
is a graphical user interface for database query operation.

## Table of Contents

- [Demo](https://sql-view-editor.netlify.app/)
- [Optimization](#optimization)
- [File Structure](#file-structure)

## Optimization

- Remove unused javascript code
- Code Splitting <br />
  Nuxt (with the help of webpack) will automatically create a JavaScript file for each page.
- Reduce bundle size of vendor <br /> A simple static asset compression module that runs Gzip and Brotli compression during the build process. This is significantly more efficient than compressing files on the fly, especially for Brotli compression, which sacrifices CPU time for greater compression. <br />

  Getting Started :

  1. Install the module

     npm install -D nuxt-compress

  2. Add nuxt-compress to your buildModules in nuxt.config.js

     module.exports = {
     buildModules: ['nuxt-compress'],
     }

  3. Configure nuxt.config.js

     module.exports = {
     modules: [
     [
     'nuxt-compress',
     {
     gzip: {
     threshold: 8192,
     },
     brotli: {
     threshold: 8192,
     },
     },
     ],
     ],
     };

  See the [Nuxt configuration docs](https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-render#compressor) for more information.

<br />

## File Structure

This application was bootstrapped by [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/). <br>
Within the download you'll find the following directories and files:

    |-- SQL View Editor
        |-- README.md
        |-- nuxt.config.js
        |-- static.json
        |-- jsconfig.json
        |-- tailwind.config.js
        |-- package.json
        |-- .gitignore
        |-- .eslintrc.js
        |-- .eslintignore
        |-- components
        |   |-- Atoms
        |   |   |-- Table
        |   |   |   |-- index.vue
        |   |   |-- TailwindGroup
        |   |   |   |-- index.vue
        |   |   |-- TailwindRule
        |   |   |   |-- index.vue
        |   |-- Molecules
        |   |   |-- ButtonGroup
        |   |   |   |-- index.vue
        |   |   |-- Header
        |   |   |   |-- index.vue
        |   |   |-- Nav
        |   |   |   |-- index.vue
        |   |-- Organisms
        |   |   |-- Aside
        |   |   |   |-- index.vue
        |-- layouts
        |   |-- default.vue
        |-- pages
        |   |-- index.vue
        |   |-- mariadb
        |   |   |-- index.vue
        |   |-- mssql
        |   |   |-- index.vue
        |   |-- postgresql
        |   |   |-- index.vue
        |-- plugins
        |   |-- element-ui.js
        |   |-- vue-query-builder.js
        |-- static
        |   |-- favicon.png
        |-- store
        |   |-- index.js
        |   |-- products
        |   |   |-- actions.js
        |   |   |-- mutations.js
        |   |   |-- getters.js
        |   |   |-- state.js
        |-- assets
        |   |-- styles
        |   |   |-- main.scss
        |-- content
        |   |-- customers.json

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).

### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).
