module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:vue/essential',
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'vue',
  ],
  rules: {
    'import/no-extraneous-dependencies': 'off',
    'no-restricted-syntax': 'off',
    'no-unused-expressions': 'off',
    'no-param-reassign': 'off',
    'vue/no-mutating-props': 'off',
    'no-bitwise': 'off',
  },
};
